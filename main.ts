interface Pixel {
  r: number;
  g: number;
  b: number;
  a?: number;
}

interface ImageParams {
  x: number;
  y: number;
  width: number;
  height: number;
}

const SAMPLE_SIZE = 10;
const STEP = SAMPLE_SIZE * 4;

// NOTE: clamp and rgb helper function source: https://hackernoon.com/creating-a-pixelation-filter-for-creative-coding-fc6dc1d728b2
function clamp(value, min, max) {
  return Math.min(Math.max(value, Math.min(min, max)), Math.max(min, max));
}

function rgb(r, g, b) {
  if (g == undefined) g = r;
  if (b == undefined) b = r;
  return (
    'rgb(' +
    clamp(Math.round(r), 0, 255) +
    ', ' +
    clamp(Math.round(g), 0, 255) +
    ', ' +
    clamp(Math.round(b), 0, 255) +
    ')'
  );
}

/**
 * Set canvas size based on image
 * @param canvas HTMLCanvasElement
 * @param image HTMLImageElement
 */
function setCanvasSize(
  canvas: HTMLCanvasElement,
  image: HTMLImageElement
): void {
  canvas.width = image.width;
  canvas.height = image.height;
}

/**
 * Check the current pixel is white
 * the 245 value is used default instead of 255 to ignore noisy pixels
 * @param pixel Pixel
 * @param threshold Threshold for white color
 */
function isWhitePixel(pixel: Pixel, threshold = 245): boolean {
  const pixelValues: number[] = Object.keys(pixel).map(key => pixel[key]);
  return !pixelValues.some(pixelValue => pixelValue <= threshold);
}

/**
 * Get RGB values from the current position
 * @param data Array of pixels
 * @param pos Pixel position
 */
function getPixel(data: Uint8ClampedArray, pos: number): Pixel {
  return {
    r: data[pos],
    g: data[pos + 1],
    b: data[pos + 2]
  };
}

/**
 * Determine image width
 * @param imageData ImageData
 * @param topLeftPos Image top left corner position
 */
function calculateWidth(imageData: ImageData, topLeftPos: number): number {
  let width = 0;
  let pos = topLeftPos;
  let pixel = getPixel(imageData.data, pos);

  while (!isWhitePixel(pixel)) {
    pos += STEP;
    pixel = getPixel(imageData.data, pos);
    width += SAMPLE_SIZE;
  }

  return width - SAMPLE_SIZE;
}

/**
 * Determine image height
 * @param imageData ImageData
 * @param topLeftPos Image top left corner position
 */
function calculateHeight(imageData: ImageData, topLeftPos: number): number {
  let height = 0;
  let pos = topLeftPos;
  let pixel = getPixel(imageData.data, pos);

  while (!isWhitePixel(pixel)) {
    pos += imageData.width * STEP;
    pixel = getPixel(imageData.data, pos);
    height += SAMPLE_SIZE;
  }

  return height - SAMPLE_SIZE;
}

/**
 * Calculate image params which need for draw it into canvas
 * @param imageData ImageData
 * @param pos Pixel position
 */
function calculateImageParams(imageData: ImageData, pos: number): ImageParams {
  const width = calculateWidth(imageData, pos);
  const height = calculateHeight(imageData, pos);
  const x = (pos / 4) % imageData.width;
  const y = Math.floor(pos / 4 / imageData.width);

  return { x, y, width, height };
}

/**
 * Determine top left corner of an image
 * @param imageData ImageData
 * @param pos Pixel position
 */
function isTopLeftCornerPixel(imageData: ImageData, pos: number): boolean {
  // positions
  const prevPos = pos - STEP;
  const prevRowPos = pos - imageData.width * STEP;
  const prevRowPrevPos = prevRowPos - STEP;
  // pixels
  const prevPixel = getPixel(imageData.data, prevPos);
  const prevRowPixel = getPixel(imageData.data, prevRowPos);
  const prevRowPrevPixel = getPixel(imageData.data, prevRowPrevPos);

  return (
    isWhitePixel(prevPixel) &&
    isWhitePixel(prevRowPixel) &&
    isWhitePixel(prevRowPrevPixel)
  );
}

/**
 * Sort images by width
 * @param images Array of ImageParams
 */
function sortImagesByWidth(images: ImageParams[]): ImageParams[] {
  return images.sort((a, b) => a.width - b.width);
}

/**
 * Recognise images, copy them into an array and render into DOM
 * @param imageData ImageData
 * @param image HTMLImageElement
 */
function draw(imageData: ImageData, image: HTMLImageElement): void {
  const images: ImageParams[] = [];

  for (var y = 0; y < imageData.height; y += SAMPLE_SIZE) {
    for (var x = 0; x < imageData.width; x += SAMPLE_SIZE) {
      const pos = (x + y * imageData.width) * 4;
      const pixel = getPixel(imageData.data, pos);

      if (!isWhitePixel(pixel)) {
        if (isTopLeftCornerPixel(imageData, pos)) {
          const img = calculateImageParams(imageData, pos);
          images.push(img);
        }
      }
    }
  }

  renderImages(sortImagesByWidth(images), image);
}

function renderImages(images: ImageParams[], image: HTMLImageElement) {
  const bodyEl = document.body;
  const containerEl = document.createElement('div');

  bodyEl.appendChild(containerEl);

  images.forEach((img: ImageParams) => {
    const { x, y, width, height } = img;
    const canvas = document.createElement('canvas');
    const context = canvas.getContext('2d');

    canvas.setAttribute('style', 'display: block');
    canvas.width = width;
    canvas.height = height;
    context.drawImage(image, x, y, width, height, 0, 0, width, height);
    containerEl.appendChild(canvas);
  });
}

// -----------------------------------------------------------
const image = new Image();
const canvas = document.createElement('canvas');
const context = canvas.getContext('2d');

image.src = './source.jpg';

image.onload = function() {
  setCanvasSize(canvas, image);
  context.drawImage(image, 0, 0, canvas.width, canvas.height);

  const imageData = context.getImageData(0, 0, canvas.width, canvas.height);

  draw(imageData, image);
};
